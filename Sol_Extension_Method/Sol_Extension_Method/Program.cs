﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Extension_Method
{
    class Program
    {
        static void Main(string[] args)
        {
            Client clientObj = new Client();
            clientObj.Add(4, 4);
            clientObj.Substract(7, 5);
            clientObj.Multiplication(6, 4);

            
        }
    }

    public class CalculationTestClass
    {
        public void Add(int val1,int val2)
        {
            Console.WriteLine(val1 + val2);
        }

        public void Substract(int val1, int val2)
        {
            Console.WriteLine(val1 - val2);
        }
    }

    public static class Test
    {
        public static void Multiplication(this CalculationTestClass calTestObj,int val1,int val2)
        {
            Console.WriteLine(val1 * val2);
        }
    }

    public class Client
    {
        #region  declaration

        private readonly CalculationTestClass calTestObj = null;

        #endregion

        #region  constructor

        public Client()
        {
            calTestObj = new CalculationTestClass();
        }           

        #endregion

        #region  public methods

        public void Add(int val1,int val2)
        {
            calTestObj?.Add(val1, val2);
        }

        public void Substract(int val1, int val2)
        {
            calTestObj?.Substract(val1, val2);
        }

        public void Multiplication(int val1,int val2)
        {
            calTestObj?.Multiplication(val1, val2);
        }

        #endregion
    }
}
